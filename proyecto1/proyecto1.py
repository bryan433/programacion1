import pygame
import sys
import time
from pygame.locals import *
pygame.init()

def tamano_nombre_ventana():
	ventana = pygame.display.set_mode((1000,768))
	pygame.display.set_caption("Chefmouse")
	return ventana

def colores_ventana():
	color_fondo_ventana = pygame.Color(255,255,255)
	color_camino = pygame.Color(0,0,0)
	return color_fondo_ventana, color_camino

def camino_laberinto(ventana,color_camino):	
	camino1 = pygame.draw.rect (ventana,color_camino,(20,10,80,20))
	camino2 = pygame.draw.rect (ventana,color_camino,(80,10,30,80))
	camino3 = pygame.draw.rect (ventana,color_camino,(13,70,137,30))
	camino4 = pygame.draw.rect (ventana,color_camino,(120,100,30,100))
	camino5 = pygame.draw.rect (ventana,color_camino,(13,100,30,100))
# firts separating
	camino6 = pygame.draw.rect (ventana,color_camino,(13,100,30,167))
	camino7 = pygame.draw.rect (ventana,color_camino,(20,160,87,28))
	camino8 = pygame.draw.rect (ventana,color_camino,(80,160,30,100))
	camino9 = pygame.draw.rect (ventana,color_camino,(80,200,30,247))
	camino10 = pygame.draw.rect (ventana,color_camino,(140,170,160,30))
# 11
	camino11 = pygame.draw.rect (ventana,color_camino,(80,310,140,30))
	camino12 = pygame.draw.rect (ventana,color_camino,(207,247,30,150))
	camino13 = pygame.draw.rect (ventana,color_camino,(207,130,30,130))
	camino14 = pygame.draw.rect (ventana,color_camino,(200,310,300,30))
	camino15 = pygame.draw.rect (ventana,color_camino,(293,170,30,280))
	camino16 = pygame.draw.rect (ventana,color_camino,(310,240,350,30))
	camino17 = pygame.draw.rect (ventana,color_camino,(500,250,30,280))
	camino18 = pygame.draw.rect (ventana,color_camino,(293,430,180,30))
	camino19 = pygame.draw.rect (ventana,color_camino,(500,360,30,200))
	camino20 = pygame.draw.rect (ventana,color_camino,(500,367,340,30))
# 21
	camino21 = pygame.draw.rect (ventana,color_camino,(760,310,30,200))
	camino22 = pygame.draw.rect (ventana,color_camino,(410,303,30,360))
	camino23 = pygame.draw.rect (ventana,color_camino,(800,450,30,200))
	camino24 = pygame.draw.rect (ventana,color_camino,(780,450,200,30))
	camino25 = pygame.draw.rect (ventana,color_camino,(960,400,30,320))
	queso_final = pygame.image.load("imagenes_laberinto/IMG-20180930-WA0008.jpg")
	posX,posY=955,680
	ventana.blit(queso_final,(posX,posY))
	raton_inicial = pygame.image.load("imagenes_laberinto/IMG-20180930-WA0007.jpg")
	posX,posY=1,5
	ventana.blit(raton_inicial,(posX,posY))
	return camino1,camino2,camino3,camino4,camino5,camino6,camino7,camino8,camino9,camino10,camino11,camino12,camino13,camino14,camino15,camino16,camino17,camino18,camino19,camino20,camino21,camino22,camino23,camino24,camino25

def imagen_movimiento():
	jugador = pygame.image.load("imagenes_laberinto/cuadrado.jpeg")
	posX,posY= 45,15
	ventana.blit(jugador,(posX,posY))
	rect_jugador=jugador.get_rect()
	return jugador, posX, posY,rect_jugador

	
def ganar(posX,posY,ventana, pasos):
	rojo = pygame.Color(250,0,0)
	if (posX == 965 and posY == 680):
		mensaje_ganar = pygame.font.Font(None, 50)
		texto_ganar = mensaje_ganar.render("Chefsito encontro su queso",0,rojo)
		mensaje_salida = pygame.font.Font(None,30)
		texto_salida = mensaje_salida.render("Presione ESC para salir",0,rojo)
		mensaje_recrear = pygame.font.Font(None,30)
		texto_recrear = mensaje_recrear.render("Recrear el juego (presione SPACE)",0,rojo)
		mensaje_volver_a_jugar = mensaje_salida.render("Presione ENTER para volver a jugar",0,rojo)
		while True:
			for event in pygame.event.get():
				if event.type == pygame.KEYDOWN:
					if event.key == K_ESCAPE:
						pygame.quit()
						sys.exit()
					elif event.key == K_SPACE:
						print ("reacrear")
						recorrer_tablero(pasos, ventana, color_fondo_ventana, color_camino)
					elif event.key == K_RETURN:
						ventana = tamano_nombre_ventana()					
						menu_juego(ventana,color_fondo_ventana)
			ventana.blit(texto_ganar,(300,0))
			ventana.blit(texto_salida,(300,90))
			ventana.blit(texto_recrear,(300,70))
			ventana.blit(mensaje_volver_a_jugar,(300,50))
			pygame.display.update()
			
def revizar_colision (ventana,color_fondo_ventana):
	rect1 = pygame.draw.rect (ventana,color_fondo_ventana,(41,5,69,5))
	rect2 = pygame.draw.rect (ventana,color_fondo_ventana,(41,35,39,5))
	#if rect_colliderect(rect_jugador):


		
def movimiento(ventana, color_fondo_ventana, color_camino, jugador, posX, posY):
	movimiento = 5
	pasos = []
	contador = 0
	while(True):
		ventana.fill(color_fondo_ventana)
		camino_laberinto(ventana,color_camino)
		revizar_colision (ventana,color_fondo_ventana)
		ventana.blit(jugador, (posX,posY))
		for event in pygame.event.get():
			print (jugador,(posX,posY))
			if (event.type == QUIT):
				pygame.quit()
				sys.exit()
			elif event.type == pygame.KEYDOWN:
					if event.key == K_LEFT:	
						if posX > 1:
							posX -= movimiento
							contador += 1
							contador = contador
					elif event.key == K_RIGHT:
						if posX < 1000:
							posX += movimiento
							contador += 1
							contador = contador
					elif event.key == K_UP:
						if posY > 0:
							posY -= movimiento
							contador += 1
							contador = contador
					elif event.key == K_DOWN:
						if posY < 1000:
							posY += movimiento
							contador += 1
							contador = contador
			elif event.type == KEYUP:
				if event.key == K_RIGHT:
					pasos.append("R")
				if event.key == K_LEFT:
					pasos.append("L")
				if event.key == K_UP:
					pasos.append("U")
				if event.key == K_DOWN:
					pasos.append("D")
		ganar(posX,posY,ventana, pasos)
		pygame.display.update()

def recorrer_tablero(pasos, ventana, color_fondo_ventana, color_camino):
	movimiento = 5
	while(True):
		ventana = tamano_nombre_ventana()
		ventana.fill(color_fondo_ventana)
		rect_jugador,jugador,posX,posY = imagen_movimiento()
		camino_laberinto(ventana, color_camino)
		ventana.blit(jugador,(posX,posY))
		for event in pygame.event.get():
			if (event.type == QUIT):
				pygame.quit()
				sys.exit()	
		for i in pasos:
			if i == 'R':
				posX += movimiento
			if i == 'L':
				posX -= movimiento
			if i == 'D':
				posY += movimiento
			if i == 'U':
				posY += movimiento
			ventana.blit(jugador,(posX,posY))
			time.sleep(0.1)
			pygame.display.update()

def menu_juego(ventana,color_fondo_ventana):
	logo = pygame.image.load("imagenes_laberinto/index.png")
	posicion_X_logo = 250
	posicion_Y_logo = 0
	ventana.fill(color_fondo_ventana)
	tamano_letra_dos = pygame.font.Font(None, 30)
	tamano_texto_juego = pygame.font.Font(None, 100)
	negro = pygame.Color(0,0,0)
	texto_entrar_juego = tamano_letra_dos.render("Presione ESPACE para entrar al juego",10,negro)
	texto_salir_juego = tamano_letra_dos.render("Presione ESC para salir",10,negro)
	texto_juego = tamano_texto_juego.render("Bienvenido a ChefMouse: ",10,negro)
	while True:
		for event in pygame.event.get():
			if event.type == pygame.KEYDOWN:
				if event.key == K_SPACE:
					jugador, posX, posY,rect_jugador = imagen_movimiento()
					movimiento(ventana, color_fondo_ventana, color_camino, jugador, posX, posY)
				elif event.key == K_ESCAPE:
					pygame.quit()
					sys.exit()
		ventana.blit(logo,(posicion_X_logo,posicion_Y_logo))
		ventana.blit(texto_juego,(80,400))
		ventana.blit(texto_entrar_juego,(300,480))
		ventana.blit(texto_salir_juego,(300,500))			
		pygame.display.update()
					
color_fondo_ventana, color_camino = colores_ventana()
ventana = tamano_nombre_ventana()					
menu_juego(ventana,color_fondo_ventana)

