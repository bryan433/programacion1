Titulo proyecto: Manejo Actividades. 

Objetivo: 
	Darle a cada usuario de la aplicación la capacidad de            		administrar sus actividades, ver su rendimientos de            		todas las actividades, analizando gráficos con el fin de 		ordenarse y la mejor disponibilidad de tiempo.

Empezando:
	Al iniciar el programa el usuario deberá crear su cuenta de 		acceso a las opciones de la aplicación, su cuenta de acceso debe 	 superar las 3 letras en cada opción.
	Al estar dentro de su cuenta deberá  ingresar actividad, podrá   		crear etiquetas, las etiquetas para poder usar deberá apretar el 		boton de añadir denuevo, o ocupar las predeterminadas.
	EL usuario, podrá eliminar, cambiarle el estado a las           	actividades, ya sea pendiente o cumplido. Existe la posibilidad 	de desconectarse para cerrar correctamente la aplicación o poder 		entrar con otra cuenta.
	
Requisitos previos:
 
	Tener Python 3 en alguna de sus versiones.
 	Sistema operativo proveniente de Linux.
	Glade instalado.
	Algún editor de texto.
	Matplotlib instalo.

Instalación: 

	Para poder ejecutar el programa debemos obtener Python en la 
	versión 3.5:
	
	wget https://www.python.org/ftp/python/3.5.0/Python-3.5.0.tgz
	tar xvf Python-3.5.0.tgz
	cd Python-3.5.0
	./configure --prefix=<directorio-a-instalar>
	make -j4
	make install



Construido con:

	Glade: Es una herramienta de desarrollo visual de interfaces 		gráficas mediante GTK/GNOME.
	Python 3.5.0: Lenguaje utilizado para la programación del     		código.
	Pep-8: Es una guía de estilo adoptada ampliamente por la    		comunidad de Python.

Especificaciones de la pep-8:

	El código se adaptó a la guia de estilo denominada pep-8, el código se basa estrictamente bajo este estilo, algunas son:
	
	# se importa la libreria time.
	import time
	# se importa la libreria json.
	import json
	# se importa la libreria Gobject Introspection (gi).
	import gi
	
	Los comentarios inician despues de el espacio correspondiente despues del '#'.
	Los import estan uno en cada línea.
	Las funciones se encuentran separadas entre dos línas vacías de código.

Versiones:

	Python 3.7.0
	Glade 3.0
Autores:

	Bryan Ahumada. – Trabajo inicial
	Benjamín Astudillo. – Trabajo inicial

Licencia:

	Este proyecto está licenciado bajo la licencia MIT. Consulte 7semanas.cl/Manejo-actividades para obtener más información.

Expresiones de gratitud:
	Empresas V & N
