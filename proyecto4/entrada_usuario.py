#! /usr/bin/env python
# -*- coding: utf-8 -*-

# se importa la libreria json.
import json
# se importa la libreria Gobject Introspection (gi).
import gi
gi.require_version('Gtk','3.0')
# import Gtk.
from gi.repository import Gtk
# se importa la libreria sys.
import sys
# se importa la libreria os.
import os
import os.path as path
import matplotlib.pyplot as plt
ETIQUETAS= ["Cocinas", "Estudiar", "Clases", "pasear al perro", "fiesta", "entrega proyecto", "reunion", "dormir"] 

# recorre cuentas
def traerArchivo ():

    cuentasJson = {}
    
    if path.exists('cuentas.json'):
        print ("\n\nCargando cuentas...\n")

        with open('cuentas.json',"r") as file:
            cuentasJson = json.load(file)

    else:
        print ("\n\nNo se han encontrado cuentas existentes...\n")

    return cuentasJson
    
    
def abrir_json_cuentas():
    
    try:
        with open('cuentas.json', 'r') as file:
            # Obtenemos en la variable data todo el contenido del archivo
            data = json.load(file)
            
    except IOError:
		
		# Si no encontramos el archivo, se devuelve una lista vacia
        data = []
    
    # se retornan los datos
    return data

    
# Método para guardar archivos en formato json    
def save_file_cuentas(data):
    
    with open('cuentas.json','w') as file:
		# se guardan todos los datos enviados en 'data' en formato Json
        json.dump(data, file, indent = 4)
    
    file.close()


# método para abrir archivos actividades.json
def abrir_json(usuario):
    try:
        with open(usuario + '.json', 'r') as file:
            # Obtenemos en la variable data todo el contenido del archivo
            data = json.load(file)
            
    except IOError:
		# Si no encontramos el archivo, se devuelve una lista vacia
        data = []
    # se retornan los datos
    return data
    
    
# Método para guardar archivos en formato json    
def save_file(data, usuario):
    with open(usuario + '.json','w') as file:
		# se guardan todos los datos enviados en 'data' en formato Json
        json.dump(data, file, indent = 4)
    file.close()


class ventana_inicio():
    
    def __init__(self):
        
        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk diseñados en nuestro archivo Glade.
        self.builder.add_from_file("interfaz.glade")
        # se le asignan los atributos del glade.
        self.ventana_inicio = self.builder.get_object("ventana_inicio")
        # Se le asigna un tamano a la ventana.
        self.ventana_inicio.set_default_size(300,300)
        self.ventana_inicio.show_all()
        # comando para destruir la ventana
        self.ventana_inicio.connect("destroy",Gtk.main_quit)
        # Botones.
        self.boton_iniciar_sesion = self.builder.get_object("iniciar_sesion")
        self.boton_crear_cuenta = self.builder.get_object("crearcuenta")
        self.boton_ayuda = self.builder.get_object("ayuda")
        # eventos clicked.
        self.boton_ayuda.connect("clicked", self.ventana_click_ayuda)     
        self.boton_crear_cuenta.connect("clicked", self.ventana_click_crear_cuenta)            
        self.boton_iniciar_sesion.connect("clicked", self.ventana_click_iniciar_sesion)

    
    def ventana_click_ayuda(self, btn = None):
        # llama la ventana de ayuda.
        z = ventana_ayuda()
        
    def ventana_click_iniciar_sesion(self, btn = None):
        # ventana para iniciar sesion.
        u = ventana_inicio_sesion(self.ventana_inicio)
    
    def ventana_click_crear_cuenta(self, btn = None):
        # ventana crear cuenta.
        w = ventana_crear_cuentaa()

            
class ventana_crear_cuentaa():
    
    def __init__(self):
		
        self.builder = Gtk.Builder()
        self.builder.add_from_file("interfaz.glade")
        self.ventana_crear = self.builder.get_object("ventana_creaar")
        # se visualiza la ventana.
        self.ventana_crear.show_all()
        # se le otorgan los atributos a los widget.
        self.boton_acept = self.builder.get_object("aceptarclick")
        self.boton_cancell = self.builder.get_object("cancelarclick")
        self.entrada_usuario = self.builder.get_object("entrada_usuario")
        self.entrada_contrasena = self.builder.get_object("entrada_contraseña")
        self.entrada_correo_electronico = self.builder.get_object("entrada_correo_electronico")
        # eventos clicked
        self.boton_acept.connect("clicked",self.boton_click_aceptar)        
        self.boton_cancell.connect("clicked",self.boton_click_cancelar)
        
    def boton_click_aceptar(self, btn = None):
		
        # se extraen los datos de las entradas.
        self.ingreso_usuario = self.entrada_usuario.get_text()
        self.ingreso_contrasena = self.entrada_contrasena.get_text()
        self.ingreso_correo_electronico = self.entrada_correo_electronico.get_text()
        
        # se recorre el texto ingresado por el usuario y obtenemos un valor numerico.
        largo_usuario = len(self.ingreso_usuario)
        largo_contrasena = len(self.ingreso_contrasena)
        largo_correo = len(self.ingreso_correo_electronico)
        # se revisa si los datos ingresados son validos.
        if largo_usuario > 3 and largo_contrasena > 3 and largo_correo > 3:

            # se guardan los datos
            w = {"usuario": self.ingreso_usuario,
                "contrasena": self.ingreso_contrasena,
                "Correo_electronico": self.ingreso_correo_electronico}
            
            z = abrir_json_cuentas()
            z.append(w)
            save_file_cuentas(z)  
            self.ventana_crear.destroy()
            
        # largo ingresado invalido.
        if largo_usuario < 3 and largo_contrasena < 3 and largo_correo < 3:
			# se llama ventana de advertencia, error.
            k = dialogo_warning()
                
    def boton_click_cancelar(self, btn = None):
        # se cierra la ventana de crear cuenta.
        self.ventana_crear.destroy()


# ventana error. 
class dialogo_warning():
	
    def __init__(self):
        
        self.builder = Gtk.Builder()
        self.builder.add_from_file("interfaz.glade")
        self.war = self.builder.get_object("warning")
        # boton.
        self.boton_back = self.builder.get_object("back")    
        # evento clicked.
        self.boton_back.connect("clicked", self.boton_click_back)  
        # se muestra ventana.
        self.war.show_all()
        
    def boton_click_back(self, btn = None):
        # se cierra ventana.
        self.war.destroy()

        	
class ventana_recuperar_contrasena():
	
    def __init__(self):
        
        self.builder = Gtk.Builder()
        self.builder.add_from_file("interfaz.glade")
        self.ventana_recu = self.builder.get_object("ventana_recu")
        # tamano de la ventana.
        self.ventana_recu.set_default_size(200,200)	
        self.ventana_recu.show_all()
        # entrada texto.
        self.entrada = self.builder.get_object("correoousuario")
        # botones.
        self.boton_codigo_nuevo = self.builder.get_object("codigonuevo")
        self.boton_acept = self.builder.get_object("acept")        
        # click botones.
        self.boton_acept.connect("clicked", self.boton_click_aceptar)  
        self.boton_codigo_nuevo.connect("clicked",self.boton_click_codigo_nuevo)
        
    def boton_click_codigo_nuevo(self, btn = None):
        self.ventana_recu.destroy()
        w = ventana_recuperar_contrasena()
        
    def boton_click_aceptar(self, btn = None):
        self.ventana_recu.destroy()
        q = revision_correo()
                
class ventana_ayuda():
    
    def __init__(self):
		
        self.builder = Gtk.Builder()
        self.builder.add_from_file("interfaz.glade")
        self.ventana_ayudaa = self.builder.get_object("ventana_ayuda")
        self.ventana_ayudaa.set_default_size(200,200)
        self.ventana_ayudaa.show_all()        
        # boton aceptar.
        self.boton_aceptar = self.builder.get_object("acepta")
        # clicked evento.
        self.boton_aceptar.connect("clicked", self.boton_click_aceptar)  
           
    def boton_click_aceptar(self, btn = None):
        # se cierra ventana.
        self.ventana_ayudaa.destroy()

class revision_correo():
    
    def __init__(self):
        
        self.builder = Gtk.Builder()
        self.builder.add_from_file("interfaz.glade")
        self.peticion = self.builder.get_object("peticion_aceptada")
        self.peticion.set_default_size(200,200)
        self.peticion.show_all()
        self.boton_atras = self.builder.get_object("atras")
        self.boton_atras.connect("clicked", self.boton_click_atras)
    
    def boton_click_atras(self,btn = None):
        self.peticion.destroy()        	

# ventana de inicio de sesion.
class ventana_inicio_sesion():
    # contructor.
    def __init__(self, ventana_inicio):
        
        self.builder = Gtk.Builder()
        self.builder.add_from_file("interfaz.glade")
        self.ventana_sesion = self.builder.get_object("ventana_sesion")
        self.ventana_sesion.set_default_size(200,150)
        self.ventana_sesion.show_all()
        # botones.
        self.boton_aceptar = self.builder.get_object("aceptaar")
        self.boton_cancelar = self.builder.get_object("cancelaar")
        self.boton_recuperar = self.builder.get_object("recu")
        # entrada de texto.
        self.contrasena = self.builder.get_object("contraseña")
        self.usuario = self.builder.get_object("usuario")
        # label vacio.
        self.mensaje_erroneo = self.builder.get_object("label1")
        self.ventana_inicio = ventana_inicio
        # eventos clicked.
        self.boton_recuperar.connect("clicked", self.boton_click_recuperar_contrasena)  
        self.boton_aceptar.connect("clicked", self.boton_click_aceptar)  
        self.boton_cancelar.connect("clicked", self.boton_click_cancelar)          
        self.ventana_kk = ventana_inicio
        
    def boton_click_recuperar_contrasena(self, btn = None):
        q = ventana_recuperar_contrasena()
        
    def boton_click_aceptar(self, btn = None):
        
        self.usuario_buscar = self.usuario.get_text()
        self.contrasena_buscar = self.contrasena.get_text()
        
        cuentasValidasJson = {}
        cuentasValidasJson = traerArchivo()
        
        flag = 0
        # recorre el Json, y busca el usuario que ingresa sesion.
        for i in range (len(cuentasValidasJson)):
            if cuentasValidasJson[i]['usuario'] == self.usuario_buscar and cuentasValidasJson[i]['contrasena'] == self.contrasena_buscar:
                p = ventana_principal_usuario(self.usuario_buscar, self.ventana_inicio)
                flag = 1
                user = i
                self.ventana_sesion.hide()
                self.ventana_kk.hide()
        if flag == 0:
			# si el usuario no se encuentra, el label vacio adquiere un texto.
            x = self.mensaje_erroneo.get_text()
            mensaje =  "Usuario o contrasena erronea"
            self.mensaje_erroneo.set_text(mensaje)
        
                  
    def boton_click_cancelar(self, btn = None):
        # cierra ventana.
        self.ventana_sesion.destroy()
            
# ventana de inico sesion aceptada.
class ventana_principal_usuario():
	
    def __init__(self, usuario_buscar, ventana_inicio):
        
        # contructor
        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk diseñados en nuestro archivo Glade
        self.builder.add_from_file("interfaz.glade")
        # se le asignan los atributos del glade
        self.ventana_usuario = self.builder.get_object("principal")
        # Se asocian los atributos a los elementos de nuestra ventana en glade 
        # botones
        
        self.boton_anadir = self.builder.get_object("anadir")
        self.boton_eliminar = self.builder.get_object("editar")
        self.boton_graficos = self.builder.get_object("graficos")
        self.boton_act_cumplida = self.builder.get_object("cumplida")
        self.boton_disconnect = self.builder.get_object("disconnect")
        
        
        # Se le da a los botones la conexion con el clicked llamando funciones
        self.boton_anadir.connect("clicked", self.dialogo_anadir)
        self.boton_eliminar.connect("clicked", self.delete_select_data)
        self.boton_disconnect.connect("clicked", self.click_desconectar)
        self.boton_act_cumplida.connect("clicked", self.actividades)
        self.boton_graficos.connect("clicked", self.arreglos)
        # Enumeracion de la tabla 
        self.listmodel = Gtk.ListStore(str, str, str, str, int, int, int, str)
        self.tree = self.builder.get_object("treeResultado")
        self.tree.set_model(model = self.listmodel)
        self.usuario = usuario_buscar
        # Creamos (en espaciado) cada una de las columnas
        self.cell = Gtk.CellRendererText()
        # ventana self
        self.ventana_inicio = ventana_inicio
        self.title = ("Actividad", "Descripcion", "Prioridad","Etiqueta", "Año", "Mes", "Dia", "Estado")
        	
        for i in range(len(self.title)):
            col = Gtk.TreeViewColumn(self.title[i],  self.cell,  text = i)
            self.tree.append_column(col)
                
        self.show_all_data()
        self.ventana_usuario.show_all()
	
		
	# funcion llama a la ventana del lunes
    def dialogo_anadir(self, btn = None):
        k = dialogo_actividad(self.usuario)
        response = k.dialogo.run()
        if response == Gtk.ResponseType.OK:
            print("Aprete OK")
            self.remove_all_data()
            self.show_all_data()
        elif response == Gtk.ResponseType.CANCEL:
            print("Aprete Cancelar")
        
    
    def show_all_data(self):
        data = abrir_json(self.usuario)
        for i in data:
            x = [x for x in i.values()]
            self.listmodel.append(x)
                        
    
    def remove_all_data(self):
        # Verificamos su aún hay datos en el modelo
        if len(self.listmodel) != 0:
            # remove all the entries in the model
            # Removemos todas en entradas (datos) en el modelo
            for i in range(len(self.listmodel)):
                iter = self.listmodel.get_iter(0)
                self.listmodel.remove(iter)
        # Printea un mensaje en la terminal alertando que el modelo es vacio
        print("Empty list")

    
    def delete_select_data(self, btn=None):
        model, it = self.tree.get_selection().get_selected()
        if model is None or it is None:
            return

        data = abrir_json(self.usuario)
        for i in data:
            if i['actividad'] == model.get_value(it, 0):
                data.remove(i)
        save_file(data, self.usuario)

        self.remove_all_data()
        self.show_all_data()
    
    
    def actividades(self, btn=None):
        model, it = self.tree.get_selection().get_selected()
        if model is None or it is None:
            return
        
        data = abrir_json(self.usuario)
        for i in data:
            if i['actividad'] == model.get_value(it, 0):
                i['Estado'] = 'C'
                
        save_file(data, self.usuario)
        self.remove_all_data()
        self.show_all_data()
        
        
    def click_desconectar(self, btn = None):
        b = ventana_inicio()
        self.ventana_usuario.hide()
        

    def arreglos(self, btn = None):
        
        names = ['act.Cumplidas', 'act.Pendientes']
        act_cumplidas = []
        act_pendientes = []
        x_C = 0
        x_P = 0
        data = abrir_json(self.usuario)
        for i in data:                        		         		
            if i['Estado'] == 'C':
                act_cumplidas.append(i)
                x_C += 1
                print (x_C)
            if i['Estado'] == 'P':
                act_pendientes.append(i)
                x_P += 1
                print (x_P)        
            
            values = [x_C, x_P]         
            
        if x_C != 0 or x_P != 0:
        
            plt.bar(names, values)
            plt.ylabel("numero de actividades")
            plt.xlabel("actividades")
            plt.title('Grafica de cantidad de actividades cumplidas y pendientes')
            plt.show()			
		         		
		         		
# llama una ventana de dialogo, con interaccion con el usuario.          
def seleccion_combo_box(combo):
    tree_iter = combo.get_active_iter()
    if tree_iter is not None:
        model = combo.get_model()
        seleccion = model[tree_iter][0]
        print (seleccion)
        
    return seleccion


class dialogo_actividad():
        
    def __init__(self, usuario):

        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk diseñados en Glade
        self.builder.add_from_file("interfaz.glade")
        self.dialogo = self.builder.get_object("dialogoingresar")
        self.dialogo.show_all()
        # Se asocian los atributos a los elementos de nuestro archivo Glade
        self.calendario = self.builder.get_object("calendario")
        self.actividad = self.builder.get_object("actividad")
        self.boton_cancel = self.builder.get_object("botoncancel")
        self.boton_acept = self.builder.get_object("botonok")
        self.prioridad = self.builder.get_object("combobox")
        self.descripcion = self.builder.get_object("descripcion")
        self.label2 = self.builder.get_object("etiqueta")
        self.boton_mas_etiqueta = self.builder.get_object("boton_editar")
        self.entradaestado = self.builder.get_object("estado")
        # Creamos evento "clicked" para los botones cancelar y aceptar
        # y se vincula a método edit_select_data 
        self.boton_cancel.connect("clicked", self.boton_cancelar)     
        self.boton_mas_etiqueta.connect("clicked", self.click_boton_mas_etiquetas) 
        
        self.boton_acept.connect("clicked", self.boton_aceptar1)  
        self.usuario = usuario
        
              
        self.etiqueta = Gtk.ListStore(str)
    
        for i in ETIQUETAS:
            self.etiqueta.append([i])
            
        self.label2.set_model(self.etiqueta)
        render_text = Gtk.CellRendererText()
        self.label2.pack_start(render_text, True)
        self.label2.add_attribute(render_text, "text", 0)
        
 
    
    def click_boton_mas_etiquetas(self,btn= None):
        dialogo = agregar_etiqueta()
        dialogo.ventana2.run()
         
      
    def boton_aceptar1(self, btn = None):
        
        Estado = self.entradaestado.get_text()
        self.asignatura = seleccion_combo_box (self.label2)
        self.priority = seleccion_combo_box(self.prioridad)
        # Al texto ingresado por pantalla, se almacenan en variable
        actividad = self.actividad.get_text()
        descripcion = self.descripcion.get_text()
        fecha = self.calendario.get_date()
        # se guardan en una lista
        w = {"actividad": actividad,
            "descripcion": descripcion,
            "prioridad": self.priority,
            "etiqueta": self.asignatura,
            "Anio": fecha[0],
            "Mes": fecha[1],
            "Dia": fecha[2],
            "Estado": Estado}
             
         
        z = abrir_json(self.usuario)
        z.append(w)
        save_file(z, self.usuario)
        self.dialogo.destroy()
        
    # Funcion con la capacidad de cerrar la ventana 'destroy'
    def boton_cancelar(self, btn = None):
        self.dialogo.destroy()


class agregar_etiqueta():

    def __init__(self):
		# contructor.
        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk diseñados en nuestro archivo Glade
        self.builder.add_from_file("interfaz.glade")
        # se le asignan los atributos del glade.
        self.ventana2 = self.builder.get_object("etiquetar")
        # botones.        
        self.boton_listo = self.builder.get_object("aplicar")
        self.boton_aniadir = self.builder.get_object("añadir")
        # entrada de texto.
        self.entrada_etiqueta = self.builder.get_object("entradaetiqueta")
        self.boton_listo.connect("clicked", self.cerrar)
        self.boton_aniadir.connect("clicked", self.boton_click_add)
        # evento clicked.
        self.ventana2.show_all()
        # se muestra la ventana.
        
                
    def boton_click_add(self, btn = None):
        self.entryetiqueta = self.entrada_etiqueta.get_text()
        etiquetas.append(self.entryetiqueta)
        self.ventana2.destroy()
        
           
    def cerrar(self, btn = None):
        self.agregar_etiquetaa.destroy()                
        	

# se crea el main
if __name__ == '__main__':
	# se llama la ventana_inicio.
    w = ventana_inicio()
    # crea iteracion que maneja eventos en GTK 
    Gtk.main()
