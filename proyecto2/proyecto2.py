# import pygame biblioteca multimedia.
import pygame
# Import sys provee acceso a funciones y objetos mantenidos por el interprete.
import sys
# Import random contiene una serie de funciones relacionadas con los valores aleatorios.
import random
# Import time proporciona funciones para trabajar con fechas y/o horas.
import time
# Import Json para codificar y decodificar datos en el formato JSON.
import json
# Import os, permite accer funcionalidades del sistema operativo.
import os
# os.patch permite saber si se puede acceder a un archivo o directorio.
import os.path as path
from pygame.locals import *
pygame.init()
# Declaracion de variables globales.
MOVIMIENTO = 5
# Colores en formato RGB.
BLACK = pygame.Color(0,0,0)
WHITE = pygame.Color(255,255,255)
RED = pygame.Color(255,0,0)
# Variable global para variar posiciones.
DADOS = random.randint (1,2)
NUMERO = random.randint(80,200)


# Funcion encargada del tamano de la ventana y se le da un nombre a la ventana.
def tamano_nombre_ventana():
    # tamaño de la ventana
    ventana = pygame.display.set_mode((700,700))
    # Se le da un nombre a la ventana ejecutable.
    pygame.display.set_caption("Chefmouse")
    # Se retorna la variable ventana.
    return ventana
	
# Funcion encargada de los diferentes mapas del laberinto.
def laberinto(ventana,BLACK,DADOS,NUMERO):
    contador_vertical = 100
    contador_horizontal = 700
    pygame.draw.line(ventana,BLACK,(50,400),(600,400),25)
    pygame.draw.line(ventana,BLACK,(600,150),(600,650),25)
    while NUMERO < 700:
        pygame.draw.line(ventana,BLACK,(NUMERO,NUMERO),(NUMERO,contador_vertical+25),25)
        pygame.draw.line(ventana,BLACK,(contador_horizontal,NUMERO * 2),(NUMERO,NUMERO * 2),25)
        NUMERO += 180
    # Se le da a conocer al programa la ubicacion de la imagen.
    queso = pygame.image.load("imagenes_laberinto/queso.jpeg")
    # Utilizacion de la variable global 'DADOS', encargada de variar el final del laberinto.
    if DADOS == 1:
        posX,posY = 585,650
        ventana.blit(queso,(posX,posY))
    elif DADOS == 2:
        posX,posY = 585,150
        ventana.blit(queso,(posX,posY))
        
# La funcion de imagen movimiento (), es crear al jugador.
def imagen_movimiento():
    # Se le da a conocer al programa la ubicacion de la imagen a utilizar.
    jugador = pygame.image.load("imagenes_laberinto/cud.png")
    posX,posY= 50,400
    # Se imprime la imagen en la ventana.
    ventana.blit(jugador,(posX,posY))
    # Se retorno la posicion del jugador, y el jugador.
    return jugador, posX, posY

# Funcion encargada de revisar si se cumple la colision en cada movimiento del jugador.
def condiciones_colisiones(posX,posY,ventana,WHITE,BLACK, jugador,pasos,MOVIMIENTO,laberintoJson):
    # Se recorren las posiciones del arreglo pasos[].
    # Se recorre a traves de la funcion 'len'.
    if (len(pasos) > 0):
        i= pasos[len(pasos)-1]
        # 'L' left.
        if i == 'L':
            posX += MOVIMIENTO
            movimiento(ventana, WHITE,BLACK, jugador, posX, posY,MOVIMIENTO,laberintoJson)
        # 'R' right.
        if i == 'R':
            posX -= MOVIMIENTO
            movimiento(ventana, WHITE,BLACK, jugador, posX, posY,MOVIMIENTO,laberintoJson)
        # 'U' up.
        if i == 'U':
            posY += MOVIMIENTO
            movimiento(ventana, WHITE,BLACK, jugador, posX, posY,MOVIMIENTO,laberintoJson)
        # 'D' down.
        if i == 'D':
            posY -= MOVIMIENTO
            movimiento(ventana, WHITE,BLACK, jugador, posX, posY,MOVIMIENTO,laberintoJson)

def traerArchivo ():

    labJson = {}
    # Solo se abre el archivo prueba.json si existe, de lo contrario se envia un mensaje por consola
    # Si existe el archivo prueba.json se cargaran todos los datos del archivo anterior en la variable 'laberintoJson'
    if path.exists('prueba.json'):
        # Contenido de arcivo Json se vuelca en la variable labJson
        with open('prueba.json',"r") as file:
            labJson = json.load(file)

    return labJson

# Funcion para crear archivo Json
def crearJson (laberintoJson, pasos, NUMERO):
    idLaberinto = 'Laberinto'+str(NUMERO)
    # Si no existe dicho idLaberinto se creara la clave laberintoJson[idLaberinto] en el diccionario
    if idLaberinto not in laberintoJson:
        laberintoJson[idLaberinto]=[]
    # con 'len' se obtiene la cantidad de ensenazas previas para el laberinto 'idLaberinto'
    nueva_ensenanza = len(laberintoJson[idLaberinto])

    laberintoJson[idLaberinto].append({
        'ensenanza'+str(nueva_ensenanza): pasos
        })

    with open('prueba.json',"w") as file:
        # Archivo se abre en "w", de esta forma si lo encuentra lo reescribe y de lo contrario lo crea.
        # Se escribe el contenido de la variable laberintoJson en el archivo 'prueba.json'
        json.dump (laberintoJson, file, indent = 4)

# Funcion encargada de revisar si el jugador llego a la posicion del queso (final).
def ganar(posX,posY,ventana,pasos,WHITE,BLACK,RED,laberintoJson):
    # Declaracion de la variable 'fuente', encargada del tamano y tipo de letra.
    fuente = pygame.font.Font(None, 30)
    # Variables encargadas de los textos en ventana.
    texto_ganar = fuente.render("Gano!",0,RED)
    texto_salida = fuente.render("ESC para salir",0,RED)
    texto_recrear = fuente.render("SPACE - recrear)",0,RED)
    mensaje_volver_a_jugar = fuente.render("ENTER - volver a jugar",0,RED)		
    # Posicion posible de ganar.
    if 	posX == 600 and posY == 645:
        while True:
			# Declaracion del tipo de evento KEYDOWN.
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
					# Opcion de ESCAPE, para salir del juego.
                    if event.key == K_ESCAPE:
                        pygame.quit()
                        sys.exit()
                    # Opcion de ESPACIO, para observar el recorrido del jugador.
                    elif event.key == K_SPACE:
                        recorrer_tablero(pasos, ventana, WHITE,BLACK,MOVIMIENTO,laberintoJson)
                    # Opcion de ENTER, para volver a jugar.
                    elif event.key == K_RETURN:
                        ventana = tamano_nombre_ventana()					
                        menu_juego(ventana,WHITE,BLACK)
            # Se imprimen los textos en ventana.            
            ventana.blit(texto_ganar,(300,0))
            ventana.blit(texto_salida,(300,90))
            ventana.blit(texto_recrear,(300,70))
            ventana.blit(mensaje_volver_a_jugar,(300,50))
            # Se actualiza la ventana.
            pygame.display.update()
    # Posicion posible de ganar.        
    if 	posX == 600 and posY == 185:
        while True:
            for event in pygame.event.get():
                # Declaracion del tipo de evento KEYDOWN.
                if event.type == pygame.KEYDOWN:
                    # Opcion de ESCAPE, para salir del juego.
                    if event.key == K_ESCAPE:
                        pygame.quit()
                        sys.exit()
                    # Opcion de ESPACIO, para observar el recorrido del jugador.
                    elif event.key == K_SPACE:
                        recorrer_tablero(pasos, ventana, WHITE,BLACK,MOVIMIENTO,laberintoJson)
                    # Opcion de ENTER, para volver a jugar.
                    elif event.key == K_RETURN:
                        ventana = tamano_nombre_ventana()					
                        menu_juego(ventana,WHITE,BLACK)
            # Se imprimen los textos en ventana.
            ventana.blit(texto_ganar,(300,0))
            ventana.blit(texto_salida,(300,90))
            ventana.blit(texto_recrear,(300,70))
            ventana.blit(mensaje_volver_a_jugar,(300,50))
            # Se actualiza la ventana.
            pygame.display.update()


# Funcion encargada de buscar el camino mas largo conocido.
def camino_largo(laberintoJson,idlaberinto):
    x = 0
    for i in range(len(laberinto[idlaberinto][i])):
        if x > len(laberinto[idlaberinto][i]['ensenanza' + str(i)]):
            camino = laberinto[idlaberinto][i]['ensenanza' + str(i)]
            recorrer_tablero(pasos,ventana, WHITE,BLACK,MOVIMIENTO,laberintoJson)
	
    
# Función encargada de buscar el camino mas corto ya conocido.
def mensajes_del_camino(BLACK):
    # 'fuente' encargada de del tamano y tipo de letra.
    fuente = pygame.font.Font(None, 20)
    # mensaje de la opcion.
    mensaje = fuente.render("W- camino corto",0,BLACK)
    ventana.blit(mensaje,(50,550))   
    # Declaracion variable 'fuente', encargada del tamano y tipo de letra.
    fuente = pygame.font.Font(None, 20)
    # variable que almacena el mensaje y su color correspondiente.
    mensaje = fuente.render("Q- camino largo",0,BLACK)
    ventana.blit(mensaje,(50,500))

    
# 'donde_estoy()', se encarga de la opcion de juego para saber ¿como llegue aqui?.            
def donde_estoy(BLACK):
    fuente = pygame.font.Font(None,20)
    mensaje_ayuda = fuente.render("E- ¿donde estoy?",0,BLACK)
    ventana.blit(mensaje_ayuda,(50,600)) 

# volver_jugar(), habilita la opcion de volver a jugar mientras se usa la opcion de donde_estoy.
def volver_jugar(BLACK):
    # Declaracion de la variable 'tipo_fuente', encargada del tamano y tipo de letra.
    tipo_fuente = pygame.font.Font(None,20)
    # Se crea el mensaje y se le asigna un color al texto.
    mensaje_ayuda = tipo_fuente.render("R - volver a jugar",0,BLACK)
    # Se imprime el mensaje en ventana.
    ventana.blit(mensaje_ayuda,(500,40))
    # Se actualiza la ventana.
    pygame.display.update() 

# Funcion encargada del movimiento del jugador. 						
def movimiento(ventana, WHITE,BLACK, jugador, posX, posY,MOVIMIENTO,laberintoJson):
    # Creacion del arreglo encargado de almacenar cada movimiento del jugador.
    pasos = []
    # While encargado de actualizar cada vez que se detecta un movimiento. 
    while True:
		# Se llama ventana.fill
        ventana.fill(WHITE)
        # Se llama la funcion encargada del mapa del laberinto.
        laberinto(ventana,BLACK,DADOS,NUMERO)
        ventana.blit(jugador, (posX,posY))
        # Se llaman las funciones de las preguntas.
        donde_estoy(BLACK)
        mensajes_del_camino(BLACK)
        # If encargado de la revision de cada posicion en la que se mueve el jugador.
        if ventana.get_at((posX,posY)) == (255,18,18):
			# Revisa la colision posible.
            condiciones_colisiones(posX,posY,ventana,WHITE,BLACK, jugador,pasos,MOVIMIENTO,laberintoJson)
        for event in pygame.event.get():
			# Se habilita la opcion de cerrar con el mouse la ventana mientras juega.
            if (event.type == QUIT):
                pygame.quit()
                sys.exit()
            # Se declara tipo de evento KEYDOWn.    
            elif event.type == pygame.KEYDOWN:
                    # Se habilitan las flechas del teclado.
                    # Se declaran condiciones para limitar los movimientos.
                    if event.key == K_LEFT:	
                        if posX > 1:
                            posX -= MOVIMIENTO
                            pasos.append("L")	
                    elif event.key == K_RIGHT:
                        if posX < 700:
                            posX += MOVIMIENTO
                            pasos.append("R")
                    elif event.key == K_UP:
                        if posY > 0:
                            posY -= MOVIMIENTO
                            pasos.append("U")
                    elif event.key == K_DOWN:
                        if posY < 700:
                            posY += MOVIMIENTO
                            pasos.append("D")
                    elif event.key == K_e:
						# opcion de 'donde_estoy().
                        # Se llama la funcion recorrer_tablero.
                        recorrer_tablero(pasos,ventana, WHITE,BLACK,MOVIMIENTO,laberintoJson)
                    # Opcion de cerrar el juego con la telca 'ESPACE'.
                    elif event.key == K_q:
                        print ("camino largo")
                    elif event.key == K_w:
                        print ("camino corto")					                   
                    elif event.key == K_ESCAPE:
                        pygame.quit()
                        sys.exit()
        if posX == 600 and posY == 645:
            crearJson (laberintoJson, pasos, NUMERO)
            # Se llama funcion encargada de revisar el final del juego.   		
            ganar(posX,posY,ventana, pasos,WHITE,BLACK,RED,laberintoJson)
        if posX == 600 and posY == 185:    
            crearJson (laberintoJson, pasos, NUMERO)
            # Se llama funcion encargada de revisar el final del juego.   		
            ganar(posX,posY,ventana, pasos,WHITE,BLACK,RED,laberintoJson)
        # Se actualiza la ventana.
        pygame.display.update()

# Funcion encargada de recrear los movimientos del jugador.
def recorrer_tablero(pasos,ventana, WHITE,BLACK,MOVIMIENTO,laberintoJson):
    while True:
		# Se llaman funciones constantemente.
        ventana = tamano_nombre_ventana()
        ventana.fill(WHITE)
        volver_jugar(BLACK)
        jugador,posX,posY = imagen_movimiento()
        laberinto(ventana,BLACK,DADOS,NUMERO)
        ventana.blit(jugador,(posX,posY))
        for event in pygame.event.get():
			# Se habilita la opcion de cerrar la ventana de juego con el mouse.
            if (event.type == QUIT):
                pygame.quit()
                sys.exit()
            # Tipo de evento KEYDOWN.
            elif event.type == pygame.KEYDOWN:
                # Opcion letra 'r', volver a jugar.
                if event.key == K_r:
                    ventana = tamano_nombre_ventana()					
                    movimiento(ventana, WHITE,BLACK, jugador, posX, posY,MOVIMIENTO,laberintoJson)			       
        # MOVIMIENTO como variable global.
        for i in pasos:
			# 'R' right.
            if i == 'R':
                posX += MOVIMIENTO
            # 'L' left.
            if i == 'L':
                posX -= MOVIMIENTO
            # 'D' down.
            if i == 'D':
                posY += MOVIMIENTO
            # 'U' up.
            if i == 'U':
                posY -= MOVIMIENTO
            ventana.blit(jugador,(posX,posY))
            # time.sleep pausa la ejecucion del recorrido. En este caso la retarda.
            time.sleep(0.1)
            # Se actualiza la ventana.
            pygame.display.update()
			
# Funcion del menu de juego.
def menu_juego(ventana,WHITE,BLACK):
    # se le da a conocer al programa la ubicacion de la imagen.
    logo = pygame.image.load("imagenes_laberinto/titulo.jpeg")
    # Se le asigna una posicion dentro de la ventana a la imagen.
    posicion_X_logo,posicion_Y_logo = 100,300
    ventana.fill(WHITE)
    
    # declaracion del tipo de letra y del tamano de la letra.
    fuente_letra_dos = pygame.font.Font(None, 30)
    fuente_texto_juego = pygame.font.Font(None, 70)
    
    # Textos dentro del menu.
    texto_entrar_juego = fuente_letra_dos.render("ESPACE para entrar al juego",10,BLACK)
    texto_salir_juego = fuente_letra_dos.render(" ESC para salir",10,BLACK)
    texto_juego = fuente_texto_juego.render("Chefmouse: ",10,BLACK)
    
    laberintoJson = traerArchivo ()
    
    while True:
        for event in pygame.event.get():
			# Se declara evento tipo KEYDOWN.
            if event.type == pygame.KEYDOWN:
				# Opcion 'ESPACE' para entrar al juego.
                if event.key == K_SPACE:
                    jugador, posX, posY = imagen_movimiento()
                    movimiento(ventana, WHITE,BLACK, jugador, posX, posY,MOVIMIENTO,laberintoJson)
                # Opcion 'ESCAPE' para salir del juego.
                elif event.key == K_ESCAPE:
                    pygame.quit()
                    sys.exit()
        # Se imprimen en la ventana el logo y los textos correspondientes.
        ventana.blit(logo,(posicion_X_logo,posicion_Y_logo))
        ventana.blit(texto_juego,(100,130))
        ventana.blit(texto_entrar_juego,(150,180))
        ventana.blit(texto_salir_juego,(150,200))
        # Se actualiza la ventana.
        pygame.display.update()

# Se crea el main.
if __name__ == "__main__":
	
	# Se llama la funcion ventana.					
    ventana = tamano_nombre_ventana()
    # Se llama la funcion menu_juego.					
    menu_juego(ventana,WHITE,BLACK)
