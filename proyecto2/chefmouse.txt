Titulo proyecto: Chefmouse
Objetivo:consiste en un juego de laberinto el cual, se basa en la idea de que Chefsito, personaje característico, que tiene 			  el objetivo de conseguir su queso para poder sobrevivir.
Requisitos previos: 
	Tener Python 3 en alguna de sus versiones.
 	Sistema operativo proveniente de Linux.
	Pygame instalado.
	Algún editor de texto.
Instalación: 
	Para poder ejecutar el programa debemos obtener Python en la versión 3.5:
	
	wget https://www.python.org/ftp/python/3.5.0/Python-3.5.0.tgz
	tar xvf Python-3.5.0.tgz
	cd Python-3.5.0
	./configure --prefix=<directorio-a-instalar>
	make -j4
	make install

	Instalación pygame Ubunto:
	
	sudo apt-get install python-pygam
	y luego iniciar el intérprete de Python3 y luego tratar de importar 	pygame como:
	import pygame.

	Para jugar Chefmouse, descarga el archivo del siguiente enlace:

	https://gitlab.com/Panconjamon/laberinto/tree/master/proyecto_laberinto
	
	Una vez descargado el archivo, proceder a ejecutar el archivo con 	
	algún editor de texto.
	
	Completado este paso, sólo tenemos que comenzar el juego y empezar a aventurarnos en el mundo de Chefmouse.

Construido con:

	Pygame: Librería de Python, acoplada a módulos de este.
	Python 3.5.0: Lenguaje utilizado para la programación del     		código.
	Pep-8: Es una guía de estilo adoptada ampliamente por la    		comunidad de Python.

Especificaciones de la pep-8:

	En las primeras lineas del código al importar las diferentes 		librerias que dispone el lenguaje python, al importarlas    		estas se encuentran declaradas estrictamente bajo la pep-8; 		por ejemplo:
	Al declarar las bibliotecas deben estar una en cada linea
	import pygame
	import os
	
	Durante la totalidad del codigo del proyecto, los comentarios 		se encuentran escritos bajo el estilo de la pep-8.
	por ejemplo:
	# separado el comentario por un espacio de la 
	almohadilla o numeral.
	
	Cada función que se crea durante el código se encuentra 	separada estrictamente una de otra por dos líneas de código.
	un ejemplo es:

	def camino_largo(BLACK):
    	   fuente = pygame.font.Font(None, 20)
           mensaje = fuente.render("Q- camino largo",0,BLACK)
           ventana.blit(mensaje,(50,500))
           pygame.display.update()
	# linea 1
	# linea 2    
	def camino_corto(BLACK):
           fuente = pygame.font.Font(None, 20)
           mensaje = fuente.render("W- camino corto",0,BLACK)
           ventana.blit(mensaje,(50,550))
           pygame.display.update()

	Las variables que se utilizan de manera global dentro del 		código, bajo la pep-8 deben estar estrictamente con MAYUSCULA, 		por ejemplo:

	MOVIMIENTO = 5

	los if se encuentran asi: if event.key == K_r: sin parentesis
	Las variables se deben expresar asi: movimiento = 0
	debido al seguimiento de la pep-8.
	       
Versiones:
	Python 3.5.0
Pygame 1.9.4
Autores:
	Bryan Ahumada. – Trabajo inicial
	Benjamín Astudillo. – Trabajo inicial
Licencia:
	Este proyecto está licenciado bajo la licencia MIT. Consulte el archivo Chefmouse.com/info-12 para obtener más información.
Expresiones de gratitud:
	Empresas V & N
