#! /usr/bin/env python
# -*- coding: utf-8 -*-


# se importa la libreria time.
import datetime
# se importa la libreria json.
import json
# se importa la libreria Gobject Introspection (gi).
import gi
gi.require_version('Gtk','3.0')
# import Gtk.
from gi.repository import Gtk
# se importa la libreria sys.
import sys
# se importa la libreria os.
import os

# método para abrir archivos actividades.json
def abrir_json():
    try:
        with open('actividades.json', 'r') as file:
            # Obtenemos en la variable data todo el contenido del archivo
            data = json.load(file)
            
    except IOError:
		# Si no encontramos el archivo, se devuelve una lista vacia
        data = []
    # se retornan los datos
    return data
    
# Método para guardar archivos en formato json    
def save_file(data):
    print ("guardado")
    with open('actividades.json','w') as file:
		# se guardan todos los datos enviados en 'data' en formato Json
        json.dump(data, file, indent = 4)
    file.close()
    		
		
class ventana_principal():


    def __init__(self):
        # contructor
        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk diseñados en nuestro archivo Glade
        self.builder.add_from_file("interfaz.glade")
        # se le asignan los atributos del glade
        ventana = self.builder.get_object("ventanaprincipal")
        # Se le asigna un tamano a la ventana
        ventana.set_default_size(500, 500)
        # comando para destruir la ventana
        ventana.connect("destroy",Gtk.main_quit)
        # Se asocian los atributos a los elementos de nuestra ventana en glade 
        # botones
        self.boton_anadir = self.builder.get_object("anadir")
        self.boton_editar = self.builder.get_object("editar")
        # Se le da a los botones la conexion con el clicked llamando funciones
        self.boton_anadir.connect("clicked", self.dialogo_lunes)
        self.boton_editar.connect("clicked", self.delete_select_data)
        # Enumeracion de la tabla 
        self.listmodel = Gtk.ListStore(str, str, str, str, int, int, int)
        self.tree = self.builder.get_object("treeResultado")
        self.tree.set_model(model = self.listmodel)
        # Creamos (en espaciado) cada una de las columnas
        cell = Gtk.CellRendererText()
        now = datetime.datetime.now().time()
        if now.hour == 0 and now.minute == 0 and now.second:
            self.delete_select_data()
        title = ("Actividad 1", "Actividad 2", "Actividad 3", "Actividad 4", "Año", "Mes", "Dia")
        for i in range(len(title)):
            col = Gtk.TreeViewColumn(title[i],  cell,  text=i)
            self.tree.append_column(col)
        
        self.show_all_data()
        ventana.show_all()
	
		
	# funcion llama a la ventana del lunes
    def dialogo_lunes(self, btn = None):
        k = ventana_lunes()
        response = k.lunes.run()
        if response == Gtk.ResponseType.OK:
            print("Aprete OK")
            self.remove_all_data()
            self.show_all_data()
        elif response == Gtk.ResponseType.CANCEL:
            print("Aprete Cancelar")
        
    
    def show_all_data(self):
        data = abrir_json()
        for i in data:
            x = [x for x in i.values()]
            self.listmodel.append(x)
                        
    
    def remove_all_data(self):
        # Verificamos su aún hay datos en el modelo
        if len(self.listmodel) != 0:
            # remove all the entries in the model
            # Removemos todas en entradas (datos) en el modelo
            for i in range(len(self.listmodel)):
                iter = self.listmodel.get_iter(0)
                self.listmodel.remove(iter)
        # Printea un mensaje en la terminal alertando que el modelo es vacio
        print("Empty list")

    
    def delete_select_data(self, btn=None):
        model, it = self.tree.get_selection().get_selected()
        if model is None or it is None:
            return

        data = abrir_json()
        for i in data:
            if i['actividad 1'] == model.get_value(it, 0):
                data.remove(i)
        save_file(data)

        self.remove_all_data()
        self.show_all_data()
        
         

# llama una ventana de dialogo, con interaccion con el usuario.          
class ventana_lunes():
    
    
    def __init__(self):
        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk diseñados en Glade
        self.builder.add_from_file("interfaz.glade")
        self.lunes = self.builder.get_object("dialogo")
        self.lunes.show_all()
        # Se asocian los atributos a los elementos de nuestro archivo Glade
        self.calendario = self.builder.get_object("calendario")
        self.entrada1 = self.builder.get_object("entrada1")
        self.entrada2 = self.builder.get_object("entrada2")
        self.entrada3 = self.builder.get_object("entrada3")
        self.entrada4 = self.builder.get_object("entrada4")
        self.boton_cancellunes = self.builder.get_object("buttonCancel")
        self.boton_aceplunes = self.builder.get_object("buttonOk")
        # Creamos evento "clicked" para los botones cancelar y aceptar
        # y se vincula a método edit_select_data 
        self.boton_cancellunes.connect("clicked", self.boton_cancelar)     
        self.boton_aceplunes.connect("clicked", self.dialogo_martes)     
    
    
    def dialogo_martes(self, btn = None):
        # Al texto ingresado por pantalla, se almacenan en variable
        entrada1 = self.entrada1.get_text()
        entrada2 = self.entrada2.get_text()
        entrada3 = self.entrada3.get_text()
        entrada4 = self.entrada4.get_text()
        fecha = self.calendario.get_date()
        # se guardan en una lista
        w = {"actividad 1": entrada1,
            "actividad 2": entrada2,
            "actividad 3": entrada3,
            "actividad 4": entrada4,
            "Año": fecha[0],
            "Mes": fecha[1],
            "Dia": fecha[2]}
             
         
        z = abrir_json()
        z.append(w)
        save_file(z)
        self.lunes.destroy()
    
    
    # Funcion con la capacidad de cerrar la ventana 'destroy'
    def boton_cancelar(self, btn=None):
        self.lunes.destroy()
        


# se crea el main
if __name__ == '__main__':
    w = ventana_principal()
    # crea iteracion que maneja eventos en GTK 
    Gtk.main()

