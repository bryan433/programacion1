#! /usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk','3.0')
from gi.repository import Gtk

class ventana_principal():
    
    def __init__(self):
        
        self.builder = Gtk.Builder()
        self.builder.add_from_file("guia1.glade")
        self.window = self.builder.get_object("ventana")
        self.window.set_default_size(800,600)
        self.window.connect("destroy", Gtk.main_quit)
        self.window.show_all()
        
        self.aceptar = self.builder.get_object("apply")
        self.reset = self.builder.get_object("reset")
        self.entradatexto1 = self.builder.get_object("entrytext1")
        self.entradatexto2 = self.builder.get_object("entrytext2")
        self.entradasuma = self.builder.get_object("entrysuma")
        self.aceptar.connect("clicked", self.dialogo_info)
        self.reset.connect("clicked", self.dialogo_remover)
        
        for event in ["activate"]:
            self.entradatexto1.connect(event, self.suma_de_texto)
            self.entradatexto2.connect(event, self.suma_de_texto)
	
    def suma_de_texto(self, btn = None):
        self.texto1 = self.entradatexto1.get_text()
        self.texto2 = self.entradatexto2.get_text()
        largo_texto1 = len(self.texto1)
        largo_texto2 = len(self.texto2)
        self.suma = largo_texto1 + largo_texto2
        self.entradasuma.set_text(str(self.suma))
        
    
    def dialogo_remover(self, btn = None):
        q = dialogo_remove(self.texto1, self.texto2, self.entradatexto1, self.entradatexto2, self.suma, self.entradasuma)
    
    
    def dialogo_info(self, btn = None):
        self.suma_de_texto()
        d = dialogo_aceptar(self.texto1, self.texto2, self.entradatexto1, self.entradatexto2, self.suma, self.entradasuma)    		
     
class dialogo_aceptar():
	
    def __init__(self, texto1, texto2, entradatexto1, entradatexto2, suma, entradasuma):
		
        self.builder = Gtk.Builder()
        self.builder.add_from_file("guia1.glade")
        self.dialogo = self.builder.get_object("dialogo")
        self.dialogo.show_all()
        
        self.ok = self.builder.get_object("ok")
        self.cancel = self.builder.get_object("cancel")
        self.label1 = self.builder.get_object("label1")
        self.label2 = self.builder.get_object("label2")	
        self.label3 = self.builder.get_object("label3")
        self.ok.connect("clicked", self.boton_aceptar)
        self.cancel.connect("clicked", self.boton_cancelar)
        
        self.texto1 = texto1
        self.texto2 = texto2
        self.suma = suma
        self.entradatexto1 = entradatexto1
        self.entradatexto2 = entradatexto2
        self.entradasuma = entradasuma
        
        self.label1.set_text(self.texto1)
        self.label2.set_text(self.texto2)
        self.label3.set_text(str(self.suma))
        
      
    def boton_aceptar(self, btn = None):
        self.dialogo.destroy()        


    def boton_cancelar(self, btn = None):
        self.texto1 = ("")
        self.texto2 = ("")
        self.suma = 0
        self.entradatexto1.set_text(self.texto1)
        self.entradatexto2.set_text(self.texto2)
        self.entradasuma.set_text(str(self.suma))
        self.dialogo.destroy()
        w = dialogo_mensaje()
        
        
class dialogo_remove():
	
    def __init__(self, texto1, texto2, entradatexto1, entradatexto2, suma, entradasuma):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("guia1.glade")
        self.dialog = self.builder.get_object("dial")
        self.dialog.show_all()
        
        self.si = self.builder.get_object("yes")
        self.no = self.builder.get_object("no")
        self.si.connect("clicked", self.boton_yes)
        self.no.connect("clicked", self.boton_no)	
        
        self.texto1 = texto1
        self.texto2 = texto2
        self.suma = suma
        self.entradatexto1 = entradatexto1
        self.entradatexto2 = entradatexto2
        self.entradasuma = entradasuma
        
    def boton_yes(self, btn = None):
        self.texto1 = ("")
        self.texto2 = ("")
        self.suma = 0
        self.entradatexto1.set_text(self.texto1)
        self.entradatexto2.set_text(self.texto2)
        self.entradasuma.set_text(str(self.suma))
        self.dialog.destroy()
        w = dialogo_mensaje()		
        
        
    def boton_no(self, btn = None):
        self.dialog.destroy()    
        	
		
    


class dialogo_mensaje():
	

    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("guia1.glade")
        self.dialogo2 = self.builder.get_object("dialog")
        self.dialogo2.show_all()
        self.boton_acept = self.builder.get_object("OK")
        self.boton_acept.connect("clicked", self.cerrar_dialogo)
        
        
    def cerrar_dialogo(self, btn = None):
        self.dialogo2.destroy()        
		
		
if __name__ == "__main__":
	
    k = ventana_principal()
    Gtk.main()  
		 
		
