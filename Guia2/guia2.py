#! /usr/bin/env python
# -*- coding: utf-8 -*-

# se importa la libreria json.
import json
# se importa la libreria Gobject Introspection (gi).
import gi
gi.require_version('Gtk','3.0')
# import Gtk.
from gi.repository import Gtk
# se importa la libreria sys.
import sys
# se importa la libreria os.
import os
import os.path as path



def abrir_json():
    
    try:
        with open('guia2.json', 'r') as file:
            # Obtenemos en la variable data todo el contenido del archivo
            data = json.load(file)
            
    except IOError:
		
		# Si no encontramos el archivo, se devuelve una lista vacia
        data = []
    
    # se retornan los datos
    return data

    
# Método para guardar archivos en formato json    
def save_file(data):
    
    with open('guia2.json','w') as file:
		# se guardan todos los datos enviados en 'data' en formato Json
        json.dump(data, file, indent = 4)
    
    file.close()


def seleccion_combo_box(combo):
    tree_iter = combo.get_active_iter()
    if tree_iter is not None:
        model = combo.get_model()
        seleccion = model[tree_iter][0]
        print (seleccion)
        
    return seleccion



class ventana_principal():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("guia2.glade")
        self.window = self.builder.get_object("ventana")
        self.window.maximize()
        self.window.connect("destroy", Gtk.main_quit)
        self.boton_agregar = self.builder.get_object("agregar")
        self.boton_editar = self.builder.get_object("editar")
        self.boton_eliminar = self.builder.get_object("eliminar")
        self.entrada_compuesto = self.builder.get_object("compuesto")
        self.entrada_composicion = self.builder.get_object("quimica")
        self.tipo_compuesto = self.builder.get_object("combobox")
        
        self.boton_agregar.connect("clicked", self.agregar)
        self.boton_eliminar.connect('clicked', self.delete_select_data)
        #self.boton_editar.connect("clicked", self.editar)
        
        self.listmodel = Gtk.ListStore(str, str, str)
        self.tree = self.builder.get_object("treeResultado")
        self.tree.set_model(model = self.listmodel)
        self.cell = Gtk.CellRendererText()
        self.title = ("Nombre del compuesto", "Composicion quimica", "Tipo de compuesto")
        
        for i in range(len(self.title)):
            col = Gtk.TreeViewColumn(self.title[i],  self.cell,  text = i)
            self.tree.append_column(col)
        
        self.remove_all_data()
        self.show_all_data()
        self.window.show_all()
	    
	
    def show_all_data(self):
        data = abrir_json()
        for i in data:
            x = [x for x in i.values()]
            self.listmodel.append(x)
          
      
    def remove_all_data(self):
        # Verificamos su aún hay datos en el modelo
        if len(self.listmodel) != 0:
            # remove all the entries in the model
            # Removemos todas en entradas (datos) en el modelo
            for i in range(len(self.listmodel)):
                iter = self.listmodel.get_iter(0)
                self.listmodel.remove(iter)
        # Printea un mensaje en la terminal alertando que el modelo es vacio
        print("Empty list")
            
            
    def delete_select_data(self, btn=None):
        model, it = self.tree.get_selection().get_selected()
        if model is None or it is None:
            return

        data = abrir_json()
        for i in data:
            if i['Nombre del compuesto'] == model.get_value(it, 0):
                data.remove(i)
        save_file(data)

        self.remove_all_data()
        self.show_all_data()            
	    
	    
    def agregar(self, btn = None):
    
        self.compuesto = self.entrada_compuesto.get_text()
        self.quimico = self.entrada_composicion.get_text()
        self.tipo = seleccion_combo_box(self.tipo_compuesto)
        
        largo_compuesto = len(self.compuesto)
        largo_quimico = len(self.quimico)
        
        if largo_compuesto != 0 and largo_quimico != 0:
			
            w = {"Nombre del compuesto": self.compuesto,
                 "Composicion quimica": self.quimico,
                 "Tipo de compuesto": self.tipo}
             
            z = abrir_json()
            z.append(w)
            save_file(z)
            
            self.compuesto = ' '
            self.quimico = ' '
        
        
        
        
        
        
        
        
        
        
        
if __name__ == '__main__':

    q = ventana_principal()
    Gtk.main()             
